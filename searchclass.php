<?php
SESSION_START();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Main</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper" style="background-color: #e67e22; ">
            <ul class="sidebar-nav">
                <li class="sidebar-brand" >
                    <a href="main.php" style="color:white; border-top: 3px solid #ffffff; border-bottom: 3px solid #ffffff;">
                        Hi Zulkarnine
                    </a>
                </li>
                <li>
                    <a href="#" style="color:white;">Profile</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Class</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Task</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Planner</a>
                </li>
               
                <li>
                    <a href="#" style="color:white;">Setting</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Sign out</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper" >
            <div class="container-fluid" >
                <div class="row" >
                    <div class="col-lg-12">
                       <a href="#menu-toggle" class="" id="menu-toggle"><i class="icon-menu" style="color: #2980b9; font-size:20px;"></i></a><span style="font-size:20px;color: #2980b9; ">Add Class</span>
					<hr>
					</div>
                </div>
			    <div class="row" style="background-color:#ffffff;">
                    <div class="col-lg-12">
					<form role="form" action="response_searchclass.php" method="post">
						  <div class="form-group">
							<label for="search">Check Class</label>
							<input type="text" class="form-control" name="search" placeholder="Enter Class Name">
						  </div>
						  <button type="submit" class="btn btn-default" >Search Now</button><hr>
					</form>
					<form role="form" action="add_newclass.php" method="post">
						  <div class="form-group">
							<label for="search">or Add New Class</label>
							<input type="text" class="form-control" name="search" placeholder="Enter Class Name">
						  </div>
						  <button type="submit" class="btn btn-default" >Add Class</button>
					</form>
                 	</div>
                </div>
            </div>
        </div>
        
           
                

            
        
 
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
						
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
