<?php
SESSION_START();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Main</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

       <?php include "sidemenu.php"; ?>

        <!-- Page Content -->
        <div id="page-content-wrapper" >
            <div class="container-fluid" >
                <div class="row" >
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="" id="menu-toggle" style="width:14px;"><i class="icon-menu" style="font-size:20px; "></i><span style="font-size:20px; ">Add Task</span></a>	
					<hr>
					</div>
                </div>
			    <div class="row" style="background-color:#ffffff;">
                    <div class="col-lg-12">
					<form role="form" action="responseaddtask.php" method="post">
						  <div class="form-group">
							<label for="task">Task</label>
							<input type="text" class="form-control" name="task" placeholder="Enter Task Name">
						  </div>
						  <div class="form-group">
							<label for="Due Date">Due Date</label>
							<input type="date" class="form-control" name="duedate" placeholder="Due Date">
						  </div>
						  <div class="form-group">
							<label for="class">Select Class</label>
							<select class="form-control" name="class" placeholder="">
								<Option value="NULL">Private</option>
								<?
								include 'config.php';
								$sql= "SELECT * FROM class LEFT JOIN classmembership WHERE classmembership.user_id='".$_SESSION['user_id']."'";
								$result=mysql_query($sql);
								if(mysql_num_rows($result) > 0)
								{
									while($row = mysql_fetch_array($result))
									{
										print '<option value="'.$row['class_id'].'">'.$row['class_name'].'</option>';
									}
								}
								?>
							 </select>
						  </div>
						  <div class="form-group">
							
							<textarea name="description" style="width:100%; border-radius: 4px;" placeholder="  Task Description"></textarea>
						  </div>
						  
						  <div class="form-group" style="width:100%;">
							<label for="attachedfile">Attach File</label>
							<input type="file" name="attachedfile">
							
						  </div>

						  <button type="submit" class="btn btn-default" >Submit</button>
						</form>
                 	</div>
                </div>
            </div>
        </div>
        
           
                

            
        
 
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
						
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
