<?php
SESSION_START();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Main</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
	<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper" style="background-color: #e67e22; ">
            <ul class="sidebar-nav">
                <li class="sidebar-brand" >
                    <a href="main.php" style="color:white; border-top: 3px solid #ffffff; border-bottom: 3px solid #ffffff;">
                        Hi Zulkarnine
                    </a>
                </li>
                <li>
                    <a href="#" style="color:white;">Profile</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Class</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Task</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Planner</a>
                </li>
               
                <li>
                    <a href="#" style="color:white;">Setting</a>
                </li>
                <li>
                    <a href="#" style="color:white;">Sign out</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper" >
            <div class="container-fluid" >
                <div class="row" >
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="" id="menu-toggle"><i class="icon-menu" style="color: #2980b9; font-size:20px;"></i></a><span style="font-size:20px;color: #2980b9; ">Task Completion Rate</span>
					<hr>
					</div>
                </div>

				 <div class="row">
                        <div class="col-lg-12 ">
                           <div id="myfirstchart" style="width: 100%;height: 250px;"></div> 
                        </div><!-- ./col -->
                        
                        
                  
                    </div><!-- /.row -->
            </div>
        </div>
        
           
                

            
        
 
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
						
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
<script>
new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [

    { month: '2014-09', value: 12 },
	{ month: '2014-10', value: 8 },
    { month: '2014-11', value: 10 },
    { month: '2014-12', value: 15 }
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'month',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Value']
});
</script>
</body>

</html>
