<?php
ini_set( "display_errors", false );
date_default_timezone_set( "Asia/Kuala_Lumpur" );  
define( "DB_HOST", "localhost:8889" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "root" );
define( "DB_NAME", "unleashe_homework" );


function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );


?>
